FROM golang:alpine3.13 as base

RUN go get github.com/codegangsta/gin 
# All these steps will be cached
WORKDIR /app
COPY go.mod .
#COPY go.sum .

# Get dependancies - will also be cached if we won't change mod/sum
RUN go mod download
# COPY the source code as the last step

FROM base as dev
ENTRYPOINT sh -c "echo application at: http://$(show-ip|tr -d ' '):3000; gin --appPort 8080 --bin build/gin-bin run main.go"

FROM base as build
COPY . /app
# Build the binary
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -o /app/bin/api

FROM scratch
COPY --from=build /app/bin/api /go/bin/api
ENTRYPOINT ["/go/bin/api"]
